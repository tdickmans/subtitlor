﻿using System.Text.RegularExpressions;

namespace Subtitlor
{
    public class Show
    {
        public Regex Expression
        {
            get
            {
                return new Regex(
                    @"^((?<series_name>.+?)[\[. _-]+)?(?<season_num>\d+)x(?<ep_num>\d+)(([. _-]*x|-)(?<extra_ep_num>(?!(1080|720)[pi])(?!(?<=x)264)\d+))*[\]. _-]*((?<extra_info>.+?)((?<![. _-])-(?<release_group>[^-]+))?)?$",
                    RegexOptions.IgnoreCase);
            }
        }

        public string Id { get { return Season + Episode; } }
        public string NameOnDisk { get; set; }
        public string ShowName { get; set; }
        public string Season { get; set; }
        public string Episode { get; set; }

        public Show(string nameOnDisk)
        {
            var match = Expression.Match(nameOnDisk);

            NameOnDisk = nameOnDisk;
            ShowName = match.Groups["series_name"].Value;
            Season = match.Groups["season_num"].Value;
            Episode = match.Groups["ep_num"].Value;
        }
    }
}