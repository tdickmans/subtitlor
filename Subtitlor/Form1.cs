﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Subtitlor
{
    public partial class Subtitlor : Form
    {
        private List<Tuple<Show, Show>> _tuples;

        public Subtitlor()
        {
            _tuples = new List<Tuple<Show, Show>>();

            InitializeComponent();
        }


        // Events
        private void Subtitlor_Load(object sender, EventArgs e)
        {
            ActiveControl = label1;
        }

        private void Subtitlor_DragDrop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (ValidateDraggedInFiles(files))
                {
                    PathTb.Text = files.First();
                    RenameBtn.Enabled = true;
                    extensionTb.Text = GetExtension(files[0]);
                }
                else
                {
                    PathTb.Text = String.Empty;
                    RenameBtn.Enabled = false;
                }
            }
        }
        private void Subtitlor_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        
        private void RenameBtn_Click(object sender, EventArgs e)
        {
            LoadFiles(PathTb.Text);
            RenameSubs(PathTb.Text);
        }

        // Validation
        private bool ValidateDraggedInFiles(string[] files)
        {
            if (files.Count() != 1)
            {
                MessageBox.Show(
                    "Gelieve maximum 1 video bestand in te slepen.",
                    "Foutmelding",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                return false;
            }

            if (!IsVideo(files[0]))
            {
                MessageBox.Show(
                    "Gelieve een video bestand in te slepen.",
                    "Foutmelding",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                return false;
            }

            return true;
        }

        private bool IsVideo(string file)
        {
            string[] extensions =
            {
                ".AVI", ".MP4", ".DIVX", ".WMV", ".MKV", ".MOV", "RMVB"
            };
            return -1 != Array.IndexOf(extensions, Path.GetExtension(file).ToUpperInvariant());
        }

        // Loading + Renaming
        private void LoadFiles(string filePath)
        {
            if (File.Exists(filePath))
            {
                var parentDir = Path.GetDirectoryName(filePath);
                var allVideos = Directory.GetFiles(parentDir, "*" + extensionTb.Text).OrderBy(x => x).ToList();
                var allSubs = Directory.GetFiles(parentDir, "*" + SubExtensionTb.Text).OrderBy(x => x).ToList();

                if (allVideos.Count() == allSubs.Count())
                {
                    for (int i = 0; i < allVideos.Count(); i++)
                    {
                        _tuples.Add(new Tuple<Show, Show>(
                            new Show(allVideos[i]), new Show(allSubs[i])     
                        ));
                    }
                }
                else
                {
                    MessageBox.Show("De geselecteerde map bevat niet evenveel video's en ondertiteling bestanden.",
                    "Foutmelding",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Het geselecteerde bestand is verplaatst of bestaat niet meer",
                    "Foutmelding",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void RenameSubs(string dirName)
        {
            foreach (var tuple in _tuples)
            {
                if (File.Exists(tuple.Item1.NameOnDisk) && File.Exists(tuple.Item2.NameOnDisk))
                {
                    var video = new FileInfo(tuple.Item1.NameOnDisk);
                    var sub = new FileInfo(tuple.Item2.NameOnDisk);
                    var parentDir = video.DirectoryName;
                    var videoName = video.Name.Substring(0, video.Name.LastIndexOf('.'));
                    var newName = Path.Combine(parentDir, videoName + sub.Extension);

                    File.Move(sub.FullName, newName);
                }
            }

            MessageBox.Show(
                String.Format("{0} bestanden hernoemd", _tuples.Count()),
                "Hernoemen gelukt!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        // Helpers
        private string GetExtension(string fileName)
        {
            var pointPos = fileName.LastIndexOf('.');
            return fileName.Substring(pointPos);
        }


    }
}
