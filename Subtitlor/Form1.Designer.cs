﻿namespace Subtitlor
{
    partial class Subtitlor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PathTb = new System.Windows.Forms.TextBox();
            this.RenameBtn = new System.Windows.Forms.Button();
            this.Parameters = new System.Windows.Forms.GroupBox();
            this.SubExtensionTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.extensionTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Parameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sleep een video in de applicatie om te starten";
            // 
            // PathTb
            // 
            this.PathTb.Enabled = false;
            this.PathTb.Location = new System.Drawing.Point(15, 74);
            this.PathTb.Name = "PathTb";
            this.PathTb.Size = new System.Drawing.Size(272, 20);
            this.PathTb.TabIndex = 2;
            // 
            // RenameBtn
            // 
            this.RenameBtn.Enabled = false;
            this.RenameBtn.Location = new System.Drawing.Point(212, 109);
            this.RenameBtn.Name = "RenameBtn";
            this.RenameBtn.Size = new System.Drawing.Size(75, 23);
            this.RenameBtn.TabIndex = 3;
            this.RenameBtn.Text = "Hernoemen";
            this.RenameBtn.UseVisualStyleBackColor = true;
            this.RenameBtn.Click += new System.EventHandler(this.RenameBtn_Click);
            // 
            // Parameters
            // 
            this.Parameters.Controls.Add(this.SubExtensionTb);
            this.Parameters.Controls.Add(this.label4);
            this.Parameters.Controls.Add(this.extensionTb);
            this.Parameters.Controls.Add(this.label3);
            this.Parameters.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Parameters.Location = new System.Drawing.Point(15, 197);
            this.Parameters.Name = "Parameters";
            this.Parameters.Size = new System.Drawing.Size(272, 148);
            this.Parameters.TabIndex = 4;
            this.Parameters.TabStop = false;
            this.Parameters.Text = "Parameters";
            // 
            // SubExtensionTb
            // 
            this.SubExtensionTb.Location = new System.Drawing.Point(166, 32);
            this.SubExtensionTb.Name = "SubExtensionTb";
            this.SubExtensionTb.Size = new System.Drawing.Size(100, 22);
            this.SubExtensionTb.TabIndex = 5;
            this.SubExtensionTb.Text = ".SRT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(163, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Sub extensie";
            // 
            // extensionTb
            // 
            this.extensionTb.Enabled = false;
            this.extensionTb.Location = new System.Drawing.Point(9, 32);
            this.extensionTb.Name = "extensionTb";
            this.extensionTb.Size = new System.Drawing.Size(100, 22);
            this.extensionTb.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Video extensie";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 38);
            this.label2.TabIndex = 5;
            this.label2.Text = "Belangrijk is dat de video\'s en ondertiteling bestand in alfabetische volgorde st" +
    "aan.";
            // 
            // Subtitlor
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 357);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Parameters);
            this.Controls.Add(this.RenameBtn);
            this.Controls.Add(this.PathTb);
            this.Controls.Add(this.label1);
            this.Name = "Subtitlor";
            this.Text = "Subtitlor";
            this.Load += new System.EventHandler(this.Subtitlor_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Subtitlor_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Subtitlor_DragEnter);
            this.Parameters.ResumeLayout(false);
            this.Parameters.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PathTb;
        private System.Windows.Forms.Button RenameBtn;
        private System.Windows.Forms.GroupBox Parameters;
        private System.Windows.Forms.TextBox extensionTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SubExtensionTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
    }
}

